<?php

namespace app\controllers;

use app\models\Item;
use app\models\ItemDetails;
use app\models\Post;
use app\models\Replace;
use app\models\User;
use Kreait\Firebase\ServiceAccount;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\View;
use Kreait\Firebase\Factory;

class AutoPrintController extends \yii\rest\Controller
{
    public $identity;
    protected function verbs()
    {
        return [
            'index' => ['POST', 'OPTIONS'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS'],
                ],
            ],
        ], $behaviors);
        // return $behaviors;
    }

    public function actions()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization, content-type');
        return parent::actions();
    }

    public function actionReceive()
    {
        $order_id = 0;
        if ($dataPost = \Yii::$app->request->rawBody) {
            $dataJsonPost = json_decode($dataPost, true);
            $company = $dataJsonPost['company'][0];
            $customer = $dataJsonPost['customer'][0];
            $item = new Item();
            $item->vat = !empty($dataJsonPost['vat']) ? $dataJsonPost['vat'] : '';
            $item->order_id = !empty($dataJsonPost['order_id']) ? intval($dataJsonPost['order_id']) : '';
            $order_id = !empty($dataJsonPost['order_id']) ? intval($dataJsonPost['order_id']) : '';
            $item->bill_id = !empty($dataJsonPost['bill_id']) ? $dataJsonPost['bill_id'] : '';
            $item->order_date = !empty($dataJsonPost['order_date']) ? $dataJsonPost['order_date'] : '';
            $item->company = $item->getCompany($company['name'], $company['address'], $company['road'], $company['sub_district'], $company['district'], $company['province'], $company['zip_code'], $company['phone'], $company['tax_id']);
            $item->customer = $item->getCustomer($customer['name'], $customer['school_name'], $customer['address'], $customer['phone'], $customer['tax_id']);
            $check = $item->save();
            if ($check) {
                foreach ($dataJsonPost['data'] as $model) {
                    $item_detail = new ItemDetails();
                    $item_detail->item_id = $item->id;
                    $item_detail->name = !empty($model['name']) ? $model['name'] : '';
                    $item_detail->class = !empty($model['class']) ? $model['class'] : '';
                    $item_detail->amount = !empty($model['amount']) ? $model['amount'] : '';
                    $item_detail->price = !empty($model['price']) ? $model['price'] : '';
                    $item_detail->save();
                }
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-service-account.json');
                $firebase = (new Factory)
                    ->withServiceAccount($serviceAccount)
                    ->withDatabaseUri('https://ngautoprint.firebaseio.com')
                    ->create();
                $database = $firebase->getDatabase();
                $newPost = $database
                    ->getReference('print-order')
                    ->push([
                        'id' => $item->id,
                        'status' => true,
                        'order_id' => $order_id,
                        'create_at' => Yii::$app->formatter->asDate('now', 'php:d-m-Y')
                    ]);
                $this->actionSend($item->id);
                return [
                    'status' => 'ok'
                ];
            } else {
                return [
                    'status' => 'not save'
                ];
            }
        }
    }
    public function actionReplace()
    {
        if ($dataPost = \Yii::$app->request->rawBody) {
            $dataJsonPost = json_decode($dataPost, true);
//            $customer = $dataJsonPost[0]['customer'];
            $details = $dataJsonPost['details'];

            if (!empty($dataJsonPost)) {
                $order_id = $dataJsonPost['order_id'];
                $replace = new  Replace();
                $replace->order_replace = $dataJsonPost['order_id'];
                $replace->name = $dataJsonPost['buyer'];
                $replace->school = $dataJsonPost['school_name'];
                $replace->phone = $dataJsonPost['mobile_no'];
                $replace->date_create = $dataJsonPost['date'];
                $rp= $replace->save();
                if($rp){
                    foreach ($details as $model){
                        $replaceDetails = new  Replace();
                        $replaceDetails->replace_id =$replace->id;
                        $replaceDetails->student_name =$model['name'];
                        $replaceDetails->class =$model['class'];
                        $replaceDetails->save();
                    }
                }
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-service-account.json');
                $firebase = (new Factory)
                    ->withServiceAccount($serviceAccount)
                    ->withDatabaseUri('https://ngautoprint.firebaseio.com')
                    ->create();
                $database = $firebase->getDatabase();
                $newPost = $database
                    ->getReference('print-replace')
                    ->push([
                        'id' => $replace->id,
                        'status' => true,
                        'order_id' => $order_id,
                        'create_at' => Yii::$app->formatter->asDate('now', 'php:d-m-Y')
                    ]);
                //$this->actionSend($item->id);
                return [
                    'status' => 'ok'
                ];
            } else {
                return [
                    'status' => 'not save'
                ];
            }


        }
    }
    public function actionSend($id)
    {
        $item = Item::findOne(['id' => $id]);
        $num = ItemDetails::find()->where(['item_id' => $item->id])->count();
        $customer = json_decode($item['customer'], true);
        $date = date("d-m-Y");
        $time = date("H:i:s");
        $message = 'ใบบิล ' . $item->bill_id . ' ' . $customer['school_name'] . ' จำนวนที่สั่ง ' . $num . ' ใบ';
        $line_api = 'https://notify-api.line.me/api/notify';
        $access_token = '0axzXypzDBzUa7pjbtUvrlJjifGsbJKUeaxzIf2EXb2';
        $str = $message;    //text max 1000 ตัวอักษร
        $sticker_package_id = 2;  // Package ID ของสติกเกอร์
        $sticker_id = 150;    // ID ของสติกเกอร์
        $message_data = array(
            'message' => $str,
            'stickerPackageId' => $sticker_package_id,
            'stickerId' => $sticker_id
        );

        $result = $this->send_notify_message($line_api, $access_token, $message_data);
        //print_r($result);
    }
    public function actionTestPush()
    {
        $message = 'ใบบิล ใบทดสอบอย่าตกใจ';
        $line_api = 'https://notify-api.line.me/api/notify';
        $access_token = '0axzXypzDBzUa7pjbtUvrlJjifGsbJKUeaxzIf2EXb2';
        $str = $message;    //text max 1000 ตัวอักษร
        $sticker_package_id = 2;  // Package ID ของสติกเกอร์
        $sticker_id = 150;    // ID ของสติกเกอร์
        $message_data = array(
            'message' => $str,
            'stickerPackageId' => $sticker_package_id,
            'stickerId' => $sticker_id
        );

        $result = $this->send_notify_message($line_api, $access_token, $message_data);
        //print_r($result);
    }
    
    private function send_notify_message($line_api, $access_token, $message_data)
    {
        $headers = array('Method: POST', 'Content-type: multipart/form-data', 'Authorization: Bearer ' . $access_token);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $line_api);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        // Check Error
        if (curl_error($ch)) {
            $return_array = array('status' => '000: send fail', 'message' => curl_error($ch));
        } else {
            $return_array = json_decode($result, true);
        }
        curl_close($ch);
        return $return_array;
    }
    
    public function actionGetOrder($id){
       $item=Item::findOne(['order_id'=>$id]);
        $item_details= ItemDetails::findAll(['item_id'=>$item->id]);
        $school =Json::decode($item->customer);
        if(!empty($item)){
            return [
                'status'=>'ok',
                'order_id'=>$id,
                'item'=>$school['school_name'],
                'item_details'=>$item_details
            ];
        }else{
            return [
                'status'=>'not'
            ];
        }
    }
    public function actionGetReplace($id){
        $replace=Replace::findOne(['order_replace'=>$id]);
        if(!empty($replace)){
            $item_details= Replace::findAll(['replace_id'=>$replace->id]);
            return [
                'status'=>'ok',
                'replace_id'=>$id,
                'item'=>$replace,
                'item_details'=>$item_details
            ];
        }else{
            return [
               'status'=>'not'
            ];
        }
    }
    public function actionApp(){
        $item_id=751;
        $order_id=1632;
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-service-account.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://ngautoprint.firebaseio.com')
            ->create();
        $database = $firebase->getDatabase();
        $newPost = $database
            ->getReference('print-order')
            ->push([
                'id' => $item_id,
                'status' => true,
                'order_id' => $order_id,
                'create_at' => Yii::$app->formatter->asDate('now', 'php:d-m-Y')
            ]);
    }   
}
