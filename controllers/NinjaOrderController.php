<?php

namespace app\controllers;

use app\models\NinjaOrder;
use app\models\UploadFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NinjaOrderController implements the CRUD actions for NinjaOrder model.
 */
class NinjaOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NinjaOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UploadFile::find()->orderBy(['create_at' => SORT_DESC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NinjaOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    public function actionView($id)
    {
        $model = $this->findUploadModel($id);
        try {
            $file = Yii::getAlias('@webroot') . '/' . $model->uploadPath . '/' . $model->file;
            $inputFile = \PHPExcel_IOFactory::identify($file);
            $objReader = \PHPExcel_IOFactory::createReader($inputFile);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            Yii::$app->session->addFlash('error', 'เกิดข้อผิดพลาด' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $objWorksheet = $objPHPExcel->getActiveSheet();
        foreach ($objWorksheet->getRowIterator() as $rowIndex => $row) {
            if ($rowIndex == 1) {
            } else {
                $arr[] = $objWorksheet->rangeToArray('A' . $rowIndex . ':' . $highestColumn . $rowIndex);
                $data = $objWorksheet->rangeToArray('A' . $rowIndex . ':' . $highestColumn . $rowIndex);
                foreach ($data as $value => $items) {
                    $check_data = NinjaOrder::find()->where(['ninja_order_no' => $items[0]])->one();
                    if (empty($check_data) || $check_data == null) {
                        $data_detail = new NinjaOrder();
                        $data_detail->excel_id = $model->id;
                        $data_detail->ninja_order_no = $items[0];
                        $data_detail->name = $items[1];
                        $data_detail->address = $items[2];
                        $data_detail->postal_code = $items[3];
                        $data_detail->Email = $items[4];
                        $data_detail->phone = $items[5];
                        $data_detail->date_create = date('Y-m-d H:i:s');
                        $data_detail->status = false;
                        $data_detail->save(false);
                    }
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $arr,
            'pagination' => false,
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'allData' => $arr,
        ]);

    }

    /**
     * Creates a new NinjaOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UploadFile();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = $model->uploadFile($model, 'file');
            $model->filename = $model->file;
            $model->create_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->actionIndex();
            } else {
                echo 'Has Problem';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NinjaOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findUploadModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = $model->uploadFile($model, 'file');
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NinjaOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findUploadModel($id);
        $modelNinja = NinjaOrder::find()->where(['excel_id' => $id])->all();
        @unlink(Yii::getAlias('@webroot') . '/' . $model->uploadPath . '/' . $model->file);
        if($model->delete()){
            foreach($modelNinja as $item){
                $item->delete();
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the NinjaOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NinjaOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NinjaOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the NinjaOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UploadFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUploadModel($id)
    {
        if (($model = UploadFile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
