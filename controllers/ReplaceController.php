<?php

namespace app\controllers;

use app\models\NinjaOrder;
use app\models\Replace;
use app\models\School;
use app\models\UploadFile;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Mpdf\Mpdf;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;

class ReplaceController extends \yii\web\Controller
{

    public function actionIndex($id, $key)
    {
        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
//        $data = json_decode(file_get_contents("http://localhost:3000"), true);
        //        $rawData = $data['data'];
        if (isset($id)) {
            $dataItem = Replace::find()->where(['id' => $id])->one();
            if (isset($dataItem)) {
                $mpdf = new Mpdf([
                    'fontDir' => array_merge($fontDirs, [
                        Url::base() . 'fonts/THSarabun',
                    ]),
                    'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
                    'default_font_size' => 14,
                    'default_font' => 'thsarabun',
                    'mode' => 'utf-8',
                    'format' => 'A4',
                    'margin_top' => 95,
                    'margin_left' => 15,
                    'margin_right' => 10,
                    'margin_bottom' => 50,
                ]);
                $stylesheet = file_get_contents(Url::base() . 'css/replace.css');
                $pagesize = 20;
                $content = $this->renderPartial('contentReplace', [
                    'model' => $dataItem,
                ]);
                $footer = $this->renderPartial('_footerReplace');
                $mpdf->SetHTMLHeader($this->renderPartial('_headerReplace', [
                    'model' => $dataItem,
                ]), 2);
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->SetHTMLFooter($footer);
                $mpdf->WriteHTML($content, 2);
            }
            $path_dir = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print';
            if (!file_exists($path_dir)) {
                mkdir($path_dir, 0777);
            }
            $filename = 'replace_' . $id . '.pdf';
            $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
            // $mpdf->Output($filename, \Mpdf\Output\Destination::INLINE);
           $filename = 'replace_' . $id . '.pdf';
           $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
            // $mpdf->Output();
           $mpdf->Output($path . $filename, 'F');
        //    $py = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'python' . DIRECTORY_SEPARATOR . 'auto_print.py';
        //    $read = exec("python3 " . $py . " " . $path . " " . $filename . " " . 1);
           $read = exec('lp -d Samsung-CLX-92x1 '. $path . $filename);

           if ($read) {
                exec('rm '. $path . $filename);
                echo 'success';
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-service-account.json');
                $firebase = (new Factory)
                   ->withServiceAccount($serviceAccount)
                   ->withDatabaseUri('https://ngautoprint.firebaseio.com')
                   ->create();
                $reference = $firebase->getDatabase()->getReference('print-replace/' . $key);
                $reference->update(['status' => false]);
            } else {
                echo 'not';
            }
        } else {
            return 'fail';
        }

    }

    public function actionReportPage($id)
    {
        $replace = Replace::findOne($id);
        if (!empty($replace)) {
            $school = School::findOne(['name' => $replace->school]);
            if (!empty($school)) {
                if (!empty($school->address)) {
                    $params = Yii::$app->params;
                    $fontDirs = $params['defaultConfig']['fontDir'];
                    $fontData = $params['defaultFontConfig']['fontdata'];
                    $mpdf = new Mpdf([
                        'fontDir' => array_merge($fontDirs, [
                            Url::base() . 'fonts/THSarabun',
                        ]),
                        'fontdata' => $fontData + [
                            'thsarabun' => [
                                'R' => 'THSarabun.ttf',
                                'B' => 'THSarabun Bold.ttf',
                                'I' => 'THSarabun Italic.ttf',
                                'BI' => 'THSarabun BoldItalic.ttf',
                            ],
                        ],
                        'default_font' => 'thsarabun',
                        'mode' => 'utf-8',
                        'format' => [110, 235],
                        'language' => 'th',
                        'margin_top' => 10,
                        'margin_left' => 30,
                        'margin_right' => 15,
                        'margin_bottom' => 10,
                        'mirrorMargins' => true,
                        'orientation' => 'L',
                    ]);
                    $stylesheet = file_get_contents(Url::base() . 'css/letterStyle.css');
//        $mpdf->Image(Url::base() .'img/NGLogo.jpg', 90, 90, 0, 0, 'jpg', '', true, false);
                    $mpdf->WriteHTML($stylesheet, 1);
                    $mpdf->WriteHTML($this->renderPartial('report-page', [
                        'order' => $replace->replace_id,
                        'customer' => $replace,
                        'school' => Json::decode($school->address, true),
                    ]), 2);
                    $filename = 'letter_' . $id . '.pdf';
                    $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
                    $mpdf->Output();
                } else {
                    return $this->redirect(['school/update', 'id' => $school->id]);
                }
            }
        }
    }

    public function actionPreparePrint(){
        $post = Yii::$app->request->post();
        $aa = UploadFile::findone($post['excelid']);
        if($post['papersize'] == 1){
            $this->actionPrintAsLetter($post['size'], $post['excelid']);
        }elseif($post['papersize'] == 2){
            $this->actionPrintAsA4($post['papersize'], $post['excelid'], $post['size']);
        }elseif($post['papersize'] == 3){
            $this->actionPrintAsA5($post['papersize'], $post['excelid'], $post['size']);

        }

    }
  
    public function actionPrintAsLetter($size, $id)
    {
        $sizes;
        if($size == 1){
            $sizes = 'small / < 4 kg';
        }

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + [
                'thsarabun' => [
                    'R' => 'THSarabun.ttf',
                    'B' => 'THSarabun Bold.ttf',
                    'I' => 'THSarabun Italic.ttf',
                    'BI' => 'THSarabun BoldItalic.ttf',
                ],
            ],
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => [235, 110],
            'language' => 'th',
            'margin_top' => 5,
            'margin_left' => 20,
            'margin_right' => 5,
            'margin_bottom' => 5,
            'orientation' => 'P',
        ]);

        $detail = NinjaOrder::find()->where(['excel_id' => $id])->all();
        foreach ($detail as $value){
            $stylesheet = file_get_contents(Url::base() . 'css/letterStyle.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->WriteHTML($this->renderPartial('print-as-letter',[
                'all_print_detail' => $value,
                'weight' => $sizes
            ]), 2);
        }
        $mpdf->Output();
        $mpdf->addPage();
        // $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
    }
    
    public function actionPrintAsA4($papersize, $id, $size)
    {
        $sizes;
        if($size == 1){
            $sizes = 'small / < 4 kg';
        }
        $detail = NinjaOrder::find()->where(['excel_id' => $id])->all();

        $content = $this->renderPartial('print-as-a4',[
            'weight' => $sizes,
            'all_data' => $detail
        ]);

        $format= [
            "papersize" => 'A4',
            "content" => $content,
            "orientation" => 'L',
        ];
        
        $this->actionMpdf($format, 10, 5);
        // $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
    }

    public function actionPrintAsA5($papersize, $id, $size){
        $sizes;
        if($size == 1){
            $sizes = 'small / < 4 kg';
        }
        $detail = NinjaOrder::find()->where(['excel_id' => $id])->all();
       
        $content = $this->renderPartial('print-as-a5',[
            'weight' => $sizes,
            'all_data' => $detail
        ]);
        $format= [
            "papersize" => 'B5',
            "content" => $content,
            "orientation" => 'L',
        ];
        $this->actionMpdf($format, 30, 25);
    }


    public function actionMpdf($format, $mar_top = 0, $mar_left = 0, $mar_right = 0, $mar_bot = 0)
    {
        ["papersize" => $papersize, "content" => $content, "orientation" => $orientation] = $format;

        // var_dump($papersize);
        // exit();
        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + [
                'thsarabun' => [
                    'R' => 'THSarabun.ttf',
                    'B' => 'THSarabun Bold.ttf',
                    'I' => 'THSarabun Italic.ttf',
                    'BI' => 'THSarabun BoldItalic.ttf',
                ],
            ],
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => $papersize,
            'language' => 'th',
            'margin_top' => $mar_top,
            'margin_left' => $mar_left,
            'margin_right' => $mar_right,
            'margin_bottom' => $mar_bot,
            'orientation' => $orientation,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/letterStyle.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);
        $mpdf->Output();
        $mpdf->addPage();
    }

}