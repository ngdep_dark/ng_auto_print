<?php

namespace app\controllers;

use app\models\Item;
use app\models\ItemDetails;
use app\models\Post;
use app\models\User;
use Kreait\Firebase\ServiceAccount;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\View;
use Kreait\Firebase\Factory;

class RestApiController extends \yii\rest\Controller
{

    public $identity;

    protected function verbs()
    {
        return [
            'index' => ['POST', 'OPTIONS'],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS'],
                ],
            ],
        ], $behaviors);
        // return $behaviors;
    }

    public function actions()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization, content-type');
        return parent::actions();
    }


    public function actionRegister()
    {
        if ($dataPost = \Yii::$app->request->rawBody) {
            $dataJsonPost = json_decode($dataPost, true);
            $ref_id = '';

        }

    }

    public function actionPrintOrder()
    {
        if ($dataPost = \Yii::$app->request->rawBody) {
            $dataJsonPost = json_decode($dataPost, true);
            $company = $dataJsonPost['company'][0];
            $customer = $dataJsonPost['customer'][0];
            $item = new Item();
            $item->vat = !empty($dataJsonPost['vat']) ? $dataJsonPost['vat'] : '';
            $item->order_id = !empty($dataJsonPost['order_id']) ? intval($dataJsonPost['order_id']) : '';
            $item->bill_id = !empty($dataJsonPost['bill_id']) ? $dataJsonPost['bill_id'] : '';
            $item->order_date = !empty($dataJsonPost['order_date']) ? $dataJsonPost['order_date'] : '';
            $item->company = $item->getCompany($company['name'], $company['address'], $company['road'], $company['sub_district'], $company['district'], $company['province'], $company['zip_code'], $company['phone'], $company['tax_id']);
            $item->customer = $item->getCustomer($customer['name'], $customer['address'], $customer['phone'], $customer['tax_id']);
            $item->save();
            foreach ($dataJsonPost['data'] as $model) {
                $item_detail = new ItemDetails();
                $item_detail->item_id = $item->id;
                $item_detail->name = !empty($model['name']) ? $model['name'] : '';
                $item_detail->class = !empty($model['class']) ? $model['class'] : '';
                $item_detail->amount = !empty($model['amount']) ? $model['amount'] : '';
                $item_detail->price = !empty($model['price']) ? $model['price'] : '';
                $item_detail->save();
            }
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-service-account.json');
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://ngautoprint.firebaseio.com')
                ->create();
            $database = $firebase->getDatabase();
            $newPost = $database
                ->getReference('print-order')
                ->push([
                    'id' => $item->id,
                    'status' => true,
                    'create_at' => Yii::$app->formatter->asDate('now', 'php:d-m-Y')
                ]);
            return [
                'status' => 'processing...'
            ];
        }
    }

    public function actionGetScan()
    {
        include(__DIR__ .'/Zklib/zklibrary.php');
      //  $uploadDir = Yii::getAlias('@web/web/uploads');
        $ip = '192.168.86.24';
        $port = 4370;
        $zk = new \ZKLibrary($ip, $port);
        $con = $zk->connect();

        if (!$con) {
            echo 'Connect';
            $dates = "2018-11-03";
            $date = date("Y-m-d");
            $att = $zk->getAttendance();
            $dataArray = [];
            foreach ($att as $key => $model) {
                $dataServer = substr($model[3], 0, 10);
                if ($dataServer == $date) {
                    $dataArray[] = [
                        'key' => $model[0],
                        'user_id' => $model[1],
                        'use_scan' => $model[2],
                        'date_time' => $model[3]
                    ];
                }
            }

            $url = '';
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($dataArray),
                    'timeout' => 60
                )
            ));
            $resp = file_get_contents($url, FALSE, $context);
            print_r($resp);

        }
    }

}
