<?php

namespace app\controllers;

use app\models\Customer;
use yii\helpers\Json;
use yii\helpers\Url;
use Mpdf\Mpdf;
use Yii;
use app\models\School;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SchoolController implements the CRUD actions for School model.
 */
class SchoolController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all School models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => School::find()->orderBy('id desc'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single School model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $customer = new  Customer();
        if ($customer->load(Yii::$app->request->post())) {
            $customer->school_id = $id;
            $customer->save();
            return $this->refresh();
        }
        return $this->render('view', [
            'customer' => $customer,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new School model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new School();
        $post=Yii::$app->request->post();
        if ($model->load($post)) {
            $model->address = $model->getAddress($model['address'], $model['road'], $post['sub_district'], $post['district'], $post['province'], $post['zip_code'], $model['phone'], $model['tax_id']);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGetSchool()
    {
        $data = json_decode(file_get_contents("http://scb.futuremlm.com/api/schools"), true);
        $dataSchool = \yii\helpers\ArrayHelper::map($data, 'id', 'name');
        foreach ($data as $mo) {
            $model = new School();
            $model->name = $mo['name'];
            $model->save();
        }
        echo 'ok';
    }

    /**
     * Updates an existing School model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($post=Yii::$app->request->post())) {
            $model->address = $model->getAddress($model['address'], $model['road'], $post['sub_district'], $post['district'], $post['province'], $post['zip_code'], $model['phone'], $model['tax_id']);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCustomer()
    {

        return $this->render('customer');
    }

    /**
     * Deletes an existing School model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteCus($id, $page)
    {
        Customer::findOne($id)->delete();
        return $this->redirect(['view', 'id' => $page]);
    }

    public function actionPrint($id, $page)
    {
        $data_school=School::findOne(['id'=>$page]);
        $customer= Json::decode($data_school->address,true);
        $user =Customer::findOne(['id'=>$id]);
        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + [
                    'thsarabun' => [
                        'R' => 'THSarabun.ttf',
                        'B' => 'THSarabun Bold.ttf',
                        'I' => 'THSarabun Italic.ttf',
                        'BI' => 'THSarabun BoldItalic.ttf',
                    ]
                ],
//            'default_font_size' => 10,
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => [110, 235],
            'language' => 'th',
            'margin_top' => 10,
            'margin_left' => 30,
            'margin_right' => 15,
            'margin_bottom' => 10,
            'mirrorMargins' => true,
            'orientation' => 'L',
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/letterStyle.css');
//        $mpdf->Image(Url::base() .'img/NGLogo.jpg', 90, 90, 0, 0, 'jpg', '', true, false);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($this->renderPartial('letter', [
            'customer' => $customer,'user'=>$user,'data_school'=>$data_school]), 2);
        $filename = 'letter_' . $id . '.pdf';
        $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'file_print' . DIRECTORY_SEPARATOR;
        $mpdf->Output();
//        $mpdf->Output($path . $filename, 'F');
//        $py = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'python' . DIRECTORY_SEPARATOR . 'auto_print.py';
//        $read = exec("python3 " . $py . " " . $path . " " . $filename . " " . 2);
    }

    /**
     * Finds the School model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return School the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = School::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}