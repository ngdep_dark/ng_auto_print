<?php

use yii\db\Migration;

/**
 * Class m180809_042952_table_school
 */
class m180809_042952_table_school extends Migration
{


    public function up()
    {
        $this->createTable('school', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->text(),
        ]);
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'name' => $this->string(),
            'tel' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('school');
        $this->dropTable('customer');
    }

}
