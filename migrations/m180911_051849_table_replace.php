<?php

use yii\db\Migration;

/**
 * Class m180911_051849_table_replace
 */
class m180911_051849_table_replace extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('replace',[
            'id'=>$this->primaryKey(),
            'order_replace'=>$this->integer(),
            'name'=>$this->string(),
            'school'=>$this->string(),
            'phone'=>$this->string(),
            'replace_id'=>$this->integer(),
            'student_name'=>$this->string(),
            'class'=>$this->string(),
            'date_create'=>$this->dateTime()
        ]);
    }

    public function down()
    {
        $this->dropTable('replace');
    }
}
