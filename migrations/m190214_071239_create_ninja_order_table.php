<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ninja_order`.
 */
class m190214_071239_create_ninja_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ninja_order', [
            'id' => $this->primaryKey(),
            'excel_id' => $this->Integer(),
            'ninja_order_no' => $this->Integer(),
            'name'=>$this->string(),
            'address' => $this->string(),
            'phone'=>$this->string(),
            'postal_code'=>$this->string(),
            'Email' => $this->string(),
            'date_create'=>$this->dateTime(),
            'status' => $this->Boolean(),
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ninja_order');
    }
}
