<?php

use yii\db\Migration;

/**
 * Handles the creation of table `upload_file`.
 */
class m190214_082416_create_upload_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('upload_file', [
            'id' => $this->primaryKey(),
            'file' => $this->string(),
            'filename' => $this->string(),
            'create_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('upload_file');
    }
}
