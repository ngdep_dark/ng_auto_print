<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property int $school_id
 * @property string $name
 * @property string $tel
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id'], 'default', 'value' => null],
            [['school_id'], 'integer'],
            [['name', 'tel'], 'string', 'max' => 255],
            ['tel', 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'name' => 'ชื่อผู้รับ',
            'tel' => 'เบอร์โทร',
        ];
    }
}
