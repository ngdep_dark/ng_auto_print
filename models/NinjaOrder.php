<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ninja_order".
 *
 * @property int $id
 * @property int $ninja_order_no
 * @property string $name
 * @property string $phone
 * @property string $postal_code
 * @property string $Email
 * @property string $date_create
 * @property bool $status
 */
class NinjaOrder extends \yii\db\ActiveRecord
{

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ninja_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ninja_order_no'], 'default', 'value' => null],
            [['ninja_order_no', 'excel_id'], 'integer'],
            [['date_create'], 'safe'],
            [['status'], 'boolean'],
            [['name', 'phone', 'address', 'postal_code', 'Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ninja_order_no' => 'Ninja Order No',
            'name' => 'Name',
            'phone' => 'Phone',
            'postal_code' => 'Postal Code',
            'Email' => 'Email',
            'date_create' => 'Date Create',
            'status' => 'Status',
        ];
    }

    // public function uploadFile($model, $attribute)
    // {
    //     $file = UploadedFile::getInstance($model, $attribute);

    //     if($file){
    //         if($this->isNewRecord){
    //             $fileName = time().'_'.$file->baseName.'.'.$file->extension;
    //         }else{
    //             $fileName = $this->getOldAttribute($attribute);
    //         }
    //         $file->saveAs(Yii::getAlias('@webroot').'/'.$this->uploadPath.'/'.$fileName);

    //         return $fileName;
    //     }
    //     return $this->isNewRecord ? false : $this->getOldAttribute($attribute);
    // }

    
}
