<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "replace".
 *
 * @property int $id
 * @property int $order_replace
 * @property string $name
 * @property string $school
 * @property string $phone
 * @property int $replace_id
 * @property string $student_name
 * @property string $class
 * @property string $date_create
 */
class Replace extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'replace';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_replace', 'replace_id'], 'default', 'value' => null],
            [['order_replace', 'replace_id'], 'integer'],
            [['date_create'], 'safe'],
            [['name', 'school', 'phone', 'student_name', 'class'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_replace' => 'Order Replace',
            'name' => 'Name',
            'school' => 'School',
            'phone' => 'Phone',
            'replace_id' => 'Replace ID',
            'student_name' => 'Student Name',
            'class' => 'Class',
            'date_create' => 'Date Create',
        ];
    }
}
