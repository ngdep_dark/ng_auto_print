<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "school".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 */
class School extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school';
    }

   public  $road,$sub_district,$district,$province,$zip_code,$phone,$tax_id;

    public function rules()
    {
        return [
           [['road','sub_district','district','province','zip_code','phone','tax_id','address'],'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อโรงเรียน',
            'road'=>'ถนน',
            'sub_district'=>'ตำบล / แขวง',
            'district'=>'อำเภอ / เขต',
            'province'=>'จังหวัด',
            // 'zip_code'=>'รหัส',
            'phone'=>'เบอร์',
            'zip_code'=>'รหัสไปรษณีย์',
            'address' => 'ที่อยู่',
        ];
    }
    public function getAddress($address, $road,$sub_district,$district,$province,$zip_code,$phone,$tax_id)
    {
        $dataArray = [];
        $dataArray['address'] = !empty($address) ? $address : '';
        $dataArray['road'] = !empty($road) ? $road : '';
        $dataArray['sub_district'] = !empty($sub_district) ? $sub_district : '';
        $dataArray['district'] = !empty($district) ? $district : '';
        $dataArray['province'] = !empty($province) ? $province : '';
        $dataArray['zip_code'] = !empty($zip_code) ? $zip_code : '';
        $dataArray['phone'] = !empty($phone) ? $phone : '';
        $dataArray['tax_id'] = !empty($tax_id) ? $tax_id : '';
        return Json::encode($dataArray);
    }
    public function getShowAddress($data){

        $model = Json::decode($data,true);
        $data=' '.!empty($model['address'])?' <b>ที่อยู่.</b> '.$model['address']:' ';
        $data.=' '.!empty($model['road'])?' <b>ถนน.</b> '.$model['road']:' ';
        $data.=' '.!empty($model['sub_district'])?' <b>ต.</b> '.$model['sub_district']:' ';
        $data.=' '.!empty($model['district'])?' <b>อ.</b> '.$model['district']:' ';

        $data.=' '.!empty($model['province'])?' <b>จ.</b> '.$model['province']:' ';
        $data.=' '.!empty($model['zip_code'])?' <b>รหัสไปรษณีย์</b> '.$model['zip_code']:' ';
        $data.=' โทร'.!empty($model['phone'])?$model['phone']:'';
        return $data;

    }
}
