<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "upload_file".
 *
 * @property int $id
 * @property string $file
 * @property int $create_at
 */
class UploadFile extends \yii\db\ActiveRecord
{
    public $uploadPath = 'uploads';
    public $size_weight;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upload_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['filename'],'string', 'max' => 255 ],
            [['created_at'], 'safe'],
            [['file'], 'file', 'extensions' => 'xls,xlsx,csv', 'skipOnEmpty' => true]
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'filename' => 'ชื่อไฟล์',
            'create_at' => 'Create At',
        ];
    }

    public function uploadFile($model, $attribute)
    {
        $file = UploadedFile::getInstance($model, $attribute);

        if($file){
            if($this->isNewRecord){
                $fileName = time().'_'.$file->baseName.'.'.$file->extension;
            }else{
                $fileName = $this->getOldAttribute($attribute);
            }
            $file->saveAs(Yii::getAlias('@webroot').'/'.$this->uploadPath.'/'.$fileName);

            return $fileName;
        }
        return $this->isNewRecord ? false : $this->getOldAttribute($attribute);
    }

    public function sizeWeight(){
        return [
            1 => 'small / < 4kg',
        ];
    }

    public function optionPaper(){
        return [
            1 => 'Letter',
            2 => 'A4',
            3 => 'A5'
        ];
    }
}
