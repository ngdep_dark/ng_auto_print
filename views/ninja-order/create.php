<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NinjaOrder */

$this->title = 'Create Ninja Order';
$this->params['breadcrumbs'][] = ['label' => 'Ninja Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ninja-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
