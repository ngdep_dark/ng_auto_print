<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NinjaOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ninja Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ninja-order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ninja Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'file',
                'label' => 'ไฟล์'
            ],
            [
                'attribute' => 'create_at',
                'label' => 'สร้างเมื่อวันที่'

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'width: 80px;'],
                'template' => '{view} {delete}',
                'visibleButtons' => [
                    'view' => function ($url, $data, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye"></span>', \yii\helpers\Url::to(['/ninja-order/view', 'id' => $model['id']]));
                    },
                    'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/ninja-order/delete', 'id' => $model['id']], [
                                'data' => [
                                    'confirm' => 'คุณต้องการลบรายการนี้หรือไม่',
                                    'method' => 'post',
                                ],
                            ]);
                    }
                ]
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
