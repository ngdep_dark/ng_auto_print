<?php

use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NinjaOrder */

// var_dump($allData);
// exit();

$this->title = $model->filename;
$this->params['breadcrumbs'][] = ['label' => 'Ninja Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ninja-order-view">
    <h1><?=Html::encode($this->title)?></h1>

    <?php $form = ActiveForm::begin(['action' => ['replace/prepare-print'], 'options'=>['target'=>'_blank']])?>
    <h2>Option</h3>
    <div class='col-md-6''>
        <h3>Product Weight</h3>
        <div class="form-group">
            <span><?=Select2::widget([
    'name' => 'size',
    'data' => $model->sizeWeight(),
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);?></span>
        </div>
    </div>
    <div class='col-md-6'>
        <h3>Print Option</h3>
        <div class="form-group">
            <span><?=Select2::widget([
    'name' => 'papersize',
    'data' => $model->optionPaper(),
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);?>
        </span>
           </div>
    </div>

    <?php echo Html::hiddenInput('excelid', $model->id); ?>

    <div class="col-md-12">
       <?=Html::submitButton('Generate Print', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

<br>


<div class='col-md-12' style='margin-top: 10px'>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => 'Ninja Order No',
            'value' => function ($model) {
                return $model[0][0];
            },
        ],
        [
            'label' => 'Name',
            'value' => function ($model) {
                return $model[0][1];
            },
        ],
        [
            'label' => 'Address',
            'value' => function ($model) {
                return $model[0][2];
            },
        ],
        [
            'label' => 'Postal Code ',
            'value' => function ($model) {
                return $model[0][3];
            },
        ],
        [
            'label' => 'Email',
            'value' => function ($model) {
                return $model[0][4];
            },
        ],
        [
            'label' => 'Contact',
            'value' => function ($model) {
                return $model[0][5];
            },
        ],
    ],
])?>
</div>


</div>
