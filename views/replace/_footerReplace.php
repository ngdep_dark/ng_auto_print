<p style="font-size: 20px;">ทางบริษัท เนกเจนซอฟ จำกัดได้ดำเนินการส่งมอบบัตรประจำตัวนักเรียนเป็นที่เรียบร้อยและรับรองว่าถูกต้องทุกประการ จึงลงลายมือชื่อไว้เป็นสำคัญ</p>

<div class="row">
    <div class="col-xs-5">
        &nbsp;
    </div>
    <div class="col-xs-6 text-center">
        <p style="font-size: 20px;"><strong>ลงชื่อ </strong>........................................................................... <strong>ผู้ส่งมอบงาน </strong></p>
        <div style="font-size: 20px; margin-right: 38px;"><strong> </strong> (...........................................................................)</div>
        <div style="font-size: 20px; margin-right: 38px;"><strong>วันที่ </strong> ................./.................../...................</div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-8">
        <p style="font-size: 18px;"><strong>หมายเหตุ</strong> ..................................................................................................................................</p>
    </div>
    <div class="col-xs-2 ">
        &nbsp;
    </div>
</div>
