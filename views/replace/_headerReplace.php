<img src="img/Color.png" width="70px" class="lor" style="display: none">
<div class="pagenum">
</div>
<div class="logo">
    <img src="logo/nextgen.png">
</div>

<p class=""
   style="margin-left: 510px; margin-top: -60px;font-size: 26px; font-weight: bold"><?= 'ใบส่งมอบ Card Replace' ?></p>
<div class="row">
    <div class="col-xs-7">
        <div class="row">
            <div class="col-xs-12">
                <br>
                <b>บริษัท เนกเจนซอฟ จํากัด (สำนักงานใหญ่)</b><br>
                <p>เลขที่ 1558/41 ถ.บางนา-ตราด
                    <br>
                    แขวงบางนา เขตบางนา กรุงเทพฯ 10260
                    <br>
                    โทร. 021014822 เลขประจำตัวผู้เสียภาษี. 0105559127204
                </p>
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div style="border: 1px solid #ddd;margin-bottom: 5px"></div>
        <table width="100%" style="font-size: 20px; ">
            <tbody>
            <tr>
                <th>วันที่สั่งซื้อ</th>
                <td><?=
                    Yii::$app->formatter->asDate($model->date_create, 'php:d-m-Y');
                   ?></td>
            </tr>
            <tr>
                <th>เลขที่ใบสั่ง Replace</th>
                <td><?= $model->order_replace ?></td>
            </tr>
            </tbody>
        </table>
        <div style="border: 1px solid #ddd;margin-top: 5px"></div>
    </div>
</div>
<b>ข้อมูลผู้แจ้ง Replace</b><br>
<?php if (isset($model)): ?>
    <div style="margin-top: -1px"><strong>ชื่อลูกค้า</strong> <?= !empty($model['name']) ? $model['name'] : '-' ?></div>
    <div style="margin-top: -2px">
        <strong>ที่อยู่.&nbsp;
            &nbsp;&nbsp;</strong><span <?php if (!empty($model['school'])) ?>><?= !empty($model['school']) ? $model['school'] : '-' ?></span>
    </div>
    <p>
        <strong>เบอร์โทร. </strong> <?= !empty($model['phone']) ? $model['phone'] : '-' ?></p>
    <br>
<?php endif; ?>