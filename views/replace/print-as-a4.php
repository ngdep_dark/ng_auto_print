<?php foreach($all_data as $all_print_detail):?>
<div class='elderbordera4'>
    <div style="height: 50px;">
        <div class='qrcodea4'>
                <barcode code="NVTHNXTGS0<?=$all_print_detail['ninja_order_no']?>" type="QR" size="1" error="M" disableborder = "1" />
        </div>
        <div class='ninjabranda4'>
            <span style='font-size: 26px;font-weight: bold'>NINJA VAN</span><br>
            <span style='font-size: 14px'> AIRWAY BILL www.ninjavan.co </span>
        </div>
        <div class='barcodea4'>
            <span style='font-size: 18px; font-weight: bold'>NVTHNXTGS0<?=$all_print_detail['ninja_order_no']?></span><br>
            <barcode code="NVTHNXTGS0<?=$all_print_detail['ninja_order_no']?>" type="C128B" height="0.7" text="0.5" /><br>
            <span style='font-size: 18px; font-weight: bold'>Size/Weight: <?=' ' . $weight?></span>
        </div>
    </div>

    <div class='addressfroma4'>
        <div class='headerfrom'>FROM (SENDER)</div>
        <div class='formataddressa4'> บริษัท เนกเจนซอฟ จำกัด</div>
        <div class='formataddressa4'> 021014822 </div>
        <div class='formataddressa4'> 1558/41 ถ.เทพรัตน แขวงบางนา เขตบางนา กรุงเทพฯ 10260 </div>
    </div>

    <div class='addresstoa4'>
        <div class='headerto'>TO (ADDRESSEE)</div>
        <div class='formataddressa4'><?=$all_print_detail['name']?></div>
        <div class='formataddressa4'><?=$all_print_detail['phone']?></div>
        <div class='formataddressa4'> <?=$all_print_detail['address'] . ' ' . $all_print_detail['postal_code']?></div>
    </div>
    <div class='border coda4'>
        COD: -
    </div>

    <div class='border delivera4' >
            Deliver by:  <?=date('Y-m-d') . ' '?> 9 AM - 10PM
    </div>
</div>
<?php endforeach; ?>

