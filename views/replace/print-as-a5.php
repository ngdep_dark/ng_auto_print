<?php foreach($all_data as $all_print_detail):?>
<div class='elderbordera5' >
    <div>
        <div class="qrcodea5" >
                <barcode code="NVTHNXTGS0<?= $all_print_detail['ninja_order_no']?>" type="QR" size="1" error="M" disableborder = "1" />
        </div>
        <div class="ninjabranda5">
            <span>NINJA VAN</span><br> 
            <span> AIRWAY BILL www.ninjavan.co </span>
        </div>
        <div class="barcodea5">
            <span>NVTHNXTGS0<?= $all_print_detail['ninja_order_no'] ?></span><br>
            <barcode code="NVTHNXTGS0<?= $all_print_detail['ninja_order_no']?>" type="C128B" height="1" text="1" /><br>
            <span >Size/Weight: <?= ' '. $weight?></span>
        </div>
    </div>

    <div class="addressfroma5">
        <div class="formataddressa5" style="border-bottom: 1px solid black">FROM(SENDER)</div>
        <div class="formataddressa5"> บริษัท เนกเจนซอฟ จำกัด</div>
        <div class="formataddressa5"> 021014822 </div>
        <div class="formataddressa5"> 1558/41 ถ.เทพรัตน แขวงบางนา เขตบางนา กรุงเทพฯ 10260 </div>
    </div>

    <div class="addresstoa5">
        <div class="formataddressa5" style="border-bottom: 1px solid black">TO(ADDRESSEE)</div>
        <div class="formataddressa5"><?= $all_print_detail['name'] ?></div>
        <div class="formataddressa5"><?= $all_print_detail['phone'] ?></div>
        <div class="formataddressa5"> <?= $all_print_detail['address'] . ' ' . $all_print_detail['postal_code'] ?></div>
    </div>

    <div class="coda5"> 
        COD: -
    </div>

    <div class="delivera5">
            Deliver by:  <?= date('Y-m-d').' '?> 9 AM - 10PM
    </div>

        
    <div class="comment">
    <div style="border-bottom: 1px solid gray; padding-left: 10px">Comment</div>
    <br>
        <div> &nbsp; </div>
    
    
    </div>
        


</div>
<?php endforeach; ?>