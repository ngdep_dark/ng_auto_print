<div class='elderborder'>
    <div style="height: 50px;">
        <div class='qrcode'>
                <barcode code="NVTHNXTGS0<?= $all_print_detail['ninja_order_no']?>" type="QR" size="1.3" error="M" disableborder = "1" />
        </div>
        <div class='ninjabrand'>
            <span style='font-size: 34px;font-weight: bold'>NINJA VAN</span><br> 
            <span style='font-size: 16px'> AIRWAY BILL www.ninjavan.co </span>
        </div>
        <div class='barcode'>
            <span style='font-size: 18px; font-weight: bold'>NVTHNXTGS0<?= $all_print_detail['ninja_order_no'] ?></span><br>
            <barcode code="NVTHNXTGS0<?= $all_print_detail['ninja_order_no']?>" type="C128B" height="1" text="0.9" /><br>
            <span style='font-size: 18px; font-weight: bold'>Size/Weight: <?= ' '. $weight?></span>
        </div>
    </div>

    <div class='addressfrom'>
        <div class='headerfrom'>FROM(SENDER)</div>
        <div class='formataddress'> บริษัท เนกเจนซอฟ จำกัด</div>
        <div class='formataddress'> 021014822 </div>
        <div class='formataddress'> 1558/41 ถ.เทพรัตน แขวงบางนา เขตบางนา กรุงเทพฯ 10260 </div>
    </div>

    <div class='addressto'>
        <div class='headerto'>TO(ADDRESSEE)</div>
        <div class='formataddress'><?= $all_print_detail['name'] ?></div>
        <div class='formataddress'><?= $all_print_detail['phone'] ?></div>
        <div class='formataddress'> <?= $all_print_detail['address'] . ' ' . $all_print_detail['postal_code'] ?></div>
    </div>

    <div class='border' style='width: 320px; float: left; margin-top: 5px; height: 20px; padding-left: 10px'> 
        COD: -
    </div>

    <div class='border' style='height: 20px;float: right; width: 390px; padding-left: 20px'>
            Deliver by:  <?= date('Y-m-d').' '?> 9 AM - 10PM
    </div>

</div>