<?php
$value = [
    "name" => "บริษัท เนกเจนซอฟ จํากัด (สำนักงานใหญ่)",
    "address" => "เลขที่ 1558/41",
    "road" => "ถ.เทพรัตน",
    "sub_district" => "แขวงบางนาใต้",
    "district" => "เขตบางนา",
    "province" => "กรุงเทพฯ",
    "zip_code" => "10260",
    "phone" => "021014822",
    "tax_id" => "0105556004705"
];

?>

<div style="height: 50px">
    <div style="width: 5%;display: inline; float: left">
        <img src="img/NGLogo.jpg" style="width: 30px; height: 30px;">
    </div>
    <div style="display: inline; float: left">
        <span style="font-size: 20px; font-weight: Bold; font-style: Italic">NextSchool<span style="font-style: normal">
                <?= $value['name'] ?> </span></span><br>
    </div>
    <div>
        <span style="font-size: 20px"> <?= !empty($value['address']) ? $value['address'] : '-' ?>
            <?= !empty($value['road']) ? $value['road'] : '-' ?>
            <?= !empty($value['sub_district']) ? $value['sub_district'] : '-' ?>
            <?= !empty($value['district']) ? $value['district'] : '-' ?></span><br>
        <span style="font-size: 20px"> <?= !empty($value['province']) ? $value['province'] : '-' ?>
            &nbsp;<?= !empty($value['zip_code']) ? $value['zip_code'] : '-' ?>
            &nbsp; <?= !empty($value['phone']) ? "โทร." . $value['phone'] : '-' ?></span>
    </div>
</div>
<br>
<div style="margin-left: 250px; margin-right: 20px; height: 150px;margin-top: -3px">
    <p style="font-size: 24px; margin-top: 0px; font-weight: bold;">กรุณาส่ง <br>
        <span style="font-size: 24px; font-weight: normal"> <?=$customer['name'] ?>
            &nbsp;<span style="font-weight: bold"><?= !empty($customer['phone'])?'โทร. ' . $customer['phone']:'' ?> (
                replace# <?=$customer->order_replace?> )</span><br></span>
        <?php if (!empty($customer['school'])): ?>
        <span style="font-size: 24px; font-weight: normal">
            <?= !empty($customer['school'])?'โรงเรียน'.$customer['school']:'' ?>&nbsp;
            <br></span>
        <?php endif; ?>
        <?php if (!empty($school)): ?>
        <span style="font-size: 24px; font-weight: normal">
            <?= !empty($school['address'])?'ที่อยู่ '.$school['address'].' ':''  ?>
            <?= !empty($school['road'])?'ถ. '.$school['road']:''  ?>
            <?= !empty($school['sub_district'])?'ต. '.$school['sub_district']:''  ?>
            <?= !empty($school['district'])?'อ. '.$school['district']:''  ?>
            <?= !empty($school['province'])?'จ. '.$school['province']:''  ?>
            <br />
            <span style="font-size: 26px; font-weight: bold">
                <?= !empty($school['zip_code'])? $school['zip_code']:''  ?> </span>
            <!-- <div style="text-align: right">#=$customer->order_replace?></div> -->
        </span>
        <?php endif; ?>
    </p>
</div>