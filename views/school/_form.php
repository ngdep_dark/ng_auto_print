<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\School */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$this->registerCssFile('@web/js/type.js/dist/jquery.Thailand.min.css', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/zip.js/zip.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/JQL.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/typeahead.bundle.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dist/jquery.Thailand.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/app.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="school-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'address')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'road')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'sub_district')->textInput(['name'=>'sub_district']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'district')->textInput(['name'=>'district']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'province')->textInput(['name'=>'province']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'zip_code')->textInput(['name'=>'zip_code']) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'tax_id')->textInput() ?>
    </div>
</div>
    <div class="form-group text-right">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$data =Yii::getAlias('@web/js/db.json');
$this->registerJs('
$.Thailand({
    database: "'.$data.'",
    $district: $(\'#w0 [name="sub_district"]\'),
    $amphoe:$( "input[name=\'district\']" ),
    $province: $(\'[name="province"]\'),
    $zipcode: $(\'[name="zip_code"]\'),

    onDataFill: function(data){
        console.info(\'Data Filled\', data);
    },

    onLoad: function(){
        console.info(\'Autocomplete is ready!\');
        $(\'#loader, .demo\').toggle();
    }
});
')
?>