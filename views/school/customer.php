<?php
$this->registerCssFile('@web/js/type.js/dist/jquery.Thailand.min.css', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/zip.js/zip.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/JQL.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dependencies/typeahead.bundle.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/dist/jquery.Thailand.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/type.js/app.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="uk-container uk-padding">

    <!-- DEMO 1 -->
    <h1>Auto Complete ที่อยู่ </h1>

    <div id="loader">
        <div uk-spinner></div> รอสักครู่ กำลังโหลดฐานข้อมูล...
    </div>

    <form id="demo1" class="demo" style="display:none;" autocomplete="off" uk-grid >

        <div class="uk-width-1-2@m">
            <label class="h2">ตำบล / แขวง</label>
            <input name="district" class="uk-input uk-width-1-1" type="text">
        </div>

        <div class="uk-width-1-2@m">
            <label class="h2">อำเภอ / เขต</label>
            <input name="amphoe" class="uk-input uk-width-1-1" type="text">
        </div>

        <div class="uk-width-1-2@m">
            <label class="h2">จังหวัด</label>
            <input name="province" class="uk-input uk-width-1-1" type="text">
        </div>

        <div class="uk-width-1-2@m">
            <label class="h2">รหัสไปรษณีย์</label>
            <input name="zipcode" class="uk-input uk-width-1-1" type="text">
        </div>

    </form>



<?php
$data =Yii::getAlias('@web/js/db.json');
$this->registerJs('
$.Thailand({
    database: "'.$data.'",
    $district: $(\'#demo1 [name="district"]\'),
    $amphoe: $(\'#demo1 [name="amphoe"]\'),
    $province: $(\'#demo1 [name="province"]\'),
    $zipcode: $(\'#demo1 [name="zipcode"]\'),

    onDataFill: function(data){
        console.info(\'Data Filled\', data);
    },

    onLoad: function(){
        console.info(\'Autocomplete is ready!\');
        $(\'#loader, .demo\').toggle();
    }
});
')
?>