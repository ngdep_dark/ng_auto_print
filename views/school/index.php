<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Json;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schools';
$this->params['breadcrumbs'][] = $this->title;
$school = new  \app\models\School();
if ($post = (Yii::$app->request->post())) {
    $school->id = $post['School']['id'];
    $data_select = \app\models\School::findOne(['id' => $post['School']['id']]);
}
?>
<div class="school-index">
    <?php Pjax::begin(); ?>
    <div class="text-right">
        <?= Html::a('เพิ่มโรเรียน', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <?php GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
//                'address:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?php $form = \yii\widgets\ActiveForm::begin(['action' => 'customer', 'method' => 'post']); ?>
            <?= Select2::widget([
                'language' => 'th',
                'model' => $school,
                'attribute' => 'name',
                'data' => \yii\helpers\ArrayHelper::map(\app\models\School::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'ค้นหาโรงเรียน...'],
                'pluginEvents' => [
                    "select2:select" => "function (e) { 
                        var data = e.params.data;
                         console.log(data);
                          location.href = 'view?id='+data.id
                     }",
                    "select2:unselect" => "function() {
                                 location.href = 'index'
                          }"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    //'minimumInputLength' => 2,
                ],
            ]); ?>
            <?php \yii\widgets\ActiveForm::end(); ?>
            <?php echo  GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
//                'address:ntext',
                    ['class' => 'yii\grid\ActionColumn','template' => '{view} {delete}'],
                ],
            ]); ?>
        </div>
        <div class="col-md-8">



        </div>
    </div>


    <?php Pjax::end(); ?>
</div>