<?php
$value = ["name"=>"บริษัท เนกเจนซอฟ จํากัด (สำนักงานใหญ่)","address"=>"เลขที่ 1558/41","road"=>"ถ.บางนา-ตราด","sub_district"=>"แขวงบางนา","district"=>"เขตบางนา","province"=>"กรุงเทพฯ","zip_code"=>"10260","phone"=>"021014822","tax_id"=>"0105559127204"]



?>

<div style="height: 50px">
    <div style="width: 5%;display: inline; float: left">
        <img src="img/NGLogo.jpg" style="width: 30px; height: 30px;">
    </div>
    <div style="display: inline; float: left">
        <span style="font-size: 20px; font-weight: Bold; font-style: Italic">NextSchool<span style="font-style: normal">
                <?= $value['name'] ?> </span></span><br>
    </div>
    <div>
        <span style="font-size: 20px"> <?= !empty($value['address'])?$value['address']:'-'?>
            <?= !empty($value['road'])?$value['road']:'-'?>
            <?=!empty($value['sub_district'])?$value['sub_district']:'-'?>
            <?=!empty($value['district'])?$value['district']:'-' ?></span><br>
        <span style="font-size: 20px">
            <?=!empty($value['province'])?$value['province']:'-' ?>&nbsp;<?= !empty($value['zip_code'])?$value['zip_code']:'-'?>&nbsp;
            <?=  !empty($value['phone'])?"โทร.".$value['phone']:'-'?></span>
    </div>
</div>
<br>
<div style="margin-left: 250px; margin-right: 20px; height: 150px">
    <p style="font-size: 26px; margin-top: 0px; font-weight: bold;">กรุณาส่ง <br>
        <span style="font-size: 24px; font-weight: normal">
            <?= !empty($user['name'])?$user['name']:' '?>&nbsp;<span
                style="font-weight: bold"><?=  !empty($user['tel'])?' โทร.'.$user['tel']:' ' ?><br></span>
            <?php if(!empty($data_school->name)):?>
            <span style="font-size: 24px; font-weight: normal"> <?= 'โรงเรียน'.$data_school->name?>&nbsp;</span>
            <br></span>
        <?php endif;?>
        <?php if(!empty($customer)):?>
        <span style="font-size: 24px; font-weight: normal">
            <?=!empty( $customer['address'])? ' '.$customer['address']:' '?> </span>
        <span style="font-size: 24px; font-weight: normal">
            <?=!empty( $customer['road'])? 'ถนน.'.$customer['road']:' '?> </span>
        <span style="font-size: 24px; font-weight: normal">
            <?=!empty( $customer['district'])? 'ต. '.$customer['district']:' '?> <br></span>
        <span style="font-size: 24px; font-weight: normal">
            <?=!empty( $customer['district'])? 'อ. '.$customer['district']:' '?> </span>
        <span style="font-size: 24px; font-weight: normal">
            <?=!empty( $customer['province'])? 'จ. '.$customer['province']:' '?> </span>
        <br />
        <span style="font-size: 26px; font-weight: bold">
            <?=!empty( $customer['zip_code'])? ' '.$customer['zip_code']:' '?> </span>
        <?php endif;?>
    </p>
</div>