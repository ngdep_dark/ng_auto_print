<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\School */

$this->title = 'Update School: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
       $mock= \yii\helpers\Json::decode( $model->address,true);
    $model->address = $mock['address'];
    $model->road = $mock['road'];
    $model->sub_district = $mock['sub_district'];
    $model->district = $mock['district'];
    $model->province = $mock['province'];
    $model->zip_code = $mock['zip_code'];
    $model->phone = $mock['phone'];
    $model->tax_id = $mock['tax_id'];
    ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
