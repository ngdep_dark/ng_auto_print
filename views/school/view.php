<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\School */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;

?>
<div class="school-view">
    <h2><?= Html::encode($this->title) ?></h2>
    <div class="row">
        <div class="col-md-5">
            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>
        <div class="col-md-7">
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-md-5">
                    <?= $form->field($customer, 'name')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($customer, 'tel')->textInput() ?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <br>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                  [
                          'attribute'=>'address',
                          'format'=>'raw',
                          'value'=>function($model){
                                return $model->getShowAddress($model['address']);
                          }
                  ]
                ],
            ]) ?>
        </div>
        <div class="col-md-8">
            <?php
            $dataProvider = new ActiveDataProvider([
                'query' => \app\models\Customer::find()->where(['school_id' => $model->id]),
            ]);
            ?>
            <?php echo \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
                    'tel',
//                'address:ntext',
                    ['class' => 'yii\grid\ActionColumn', 'template' => '{print} {delete-cus}',
                        'buttons' => [
                            'print' => function ($url,$data) {
                                return Html::a('<i class="glyphicon glyphicon-envelope"></i>', ['school/print','id'=>$data['id'],'page'=>$data['school_id']], ['target' => '_blank']);
                            },
                            'delete-cus' => function ($url,$data) {
                                return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['school/delete-cus','id'=>$data['id'],'page'=>$data['school_id']]);
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>