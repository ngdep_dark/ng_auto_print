<img src="img/Color.png" width="70px" class="lor">
<div class="pagenum">
</div>
<div class="logo">
    <img src="logo/nextgen.png">
</div>

<?php if ($dataItem->vat == true): ?>
    <p class=""
       style="margin-left: 410px; margin-top: -40px;font-size: 24px; font-weight: bold"><?= 'ใบกำกับภาษี / ใบเสร็จรับเงิน' ?></p>
<?php else:; ?>
    <p class=""
       style="margin-left: 510px; margin-top: -60px;font-size: 24px; font-weight: bold"><?= 'ใบเสร็จรับเงิน' ?></p>
<?php endif; ?>
<p class="" style="margin-left:540px; font-size: 24px; font-weight: bold"><?= $pang; ?></p>
<?php
$Address = [
    'name' => 'บริษัท เนกเจนซอฟ จํากัด (สำนักงานใหญ่)',
    'address' => 'เลขที่ 1558/41',
    'road' => 'ถ.เทพรัตน',
    'sub_district' => 'แขวงบางนาใต้',
    'district'=>'เขตบางนา',
    'province'=>'กรุงเทพฯ',
    'zip_code'=>'10260',
    'phone'=>'021014822',
    'tax_id'=>'0105556004705'


];
?>
<div class="row">
    <div class="col-xs-7">
        <div class="row">
            <div class="col-xs-12">
                <br>
                <b><?= !empty($Address['name']) ? $Address['name'] : '-' ?></b><br>
                <p><?= !empty($Address['address']) ? $Address['address'] : ' ' ?> <?= !empty($Address['road']) ? $Address['road'] : ' ' ?>
                    <br>
                    <?= !empty($Address['sub_district']) ? $Address['sub_district'] : ' ' ?>
                    <?= !empty($Address['district']) ? $Address['district'] : ' ' ?>
                    <?= !empty($Address['province']) ? $Address['province'] : ' ' ?>
                    <?= !empty($Address['zip_code']) ? $Address['zip_code'] : ' ' ?>
                    <br>
                    โทร. <?= !empty($Address['phone']) ? $Address['phone'] : ' ' ?>
                    เลขประจำตัวผู้เสียภาษี. <?= !empty($Address['tax_id']) ? $Address['tax_id'] : ' ' ?>
                </p>

                <b>ข้อมูลผู้สั่งซื้อ</b><br>
                <?php if (isset($customer)): ?>
                    <div style="margin-top: 5px;">
                        <strong>ชื่อลูกค้า</strong> <?= !empty($customer['name']) ? $customer['name'] : '-' ?></div>
                    <div style="margin-top: 3px;">
                        <strong>ที่อยู่.&nbsp;
                            &nbsp;&nbsp;</strong><span <?php if (!empty($customer['school_name'])) ?>><?= !empty($customer['school_name']) ? $customer['school_name'] : '-' ?></span> <?= !empty($customer['address']) ? $customer['address'] : '-' ?>
                    </div>
                    <div style="margin-top: 3px;">
                        <strong>เบอร์โทร. </strong> <?= !empty($customer['phone']) ? $customer['phone'] : '-' ?></div>
                    <div style="margin-top: 3px;">
                        <strong>เลขประจำตัวผู้เสียภาษี </strong> <?= !empty($customer['bill_id']) ? $customer['bill_id'] : '-' ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div style="border: 1px solid #ddd;margin-bottom: 5px"></div>
        <table width="100%" style="font-size: 14px; ">
            <tbody>
            <tr>
                <th>วันที่สั่งซื้อ</th>
                <td><?= $dataItem['order_date'] ?></td>
            </tr>
            <tr>
                <?php if ($dataItem->vat == true): ?>
                    <th>เลขที่ใบกำกับภาษี</th>
                    <td><?= $dataItem['order_id'] ?></td>
                <?php else: ?>
                    <th>เลขที่ใบสั่งซื้อ</th>
                    <td><?= $dataItem['bill_id'] ?></td>
                <?php endif; ?>
            </tr>
            </tbody>
        </table>
        <div style="border: 1px solid #ddd;margin-top: 5px"></div>
    </div>
</div>