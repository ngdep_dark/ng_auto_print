<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$model=json_decode($models->customer,true);
?>

<div class="user-form">
    <div class="col-lg-offset-3 col-md-6">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($models, 'order_id')->textInput(['readonly'=> true]) ?>
    <label>ชื่อผู้สั่ง</label>
        <input type="text" name="name" id="inputID" class="form-control" value="<?=$model['name']?>" title="" required="required">
        <label>โรงเรียน</label>
        <input type="text" name="school_name" id="inputID" class="form-control" value="<?=$model['school_name']?>" title="" required="required">
        <label>ที่อยู่</label>
        <input type="text" name="address" id="inputID" class="form-control" value="<?=$model['address']?>" title="" required="required">
        <label>เบอร์โทร</label>
        <input type="text" name="phone" id="inputID" class="form-control" value="<?=$model['phone']?>" title="" >
        <label>tax_id</label>
        <input type="text" name="tax_id" id="inputID" class="form-control" value="<?=$model['tax_id']?>" title="" >
        <div class="form-group">
            <?= Html::submitButton('บันทึกแก้ไข', ['class' =>'btn btn-primary btn-block'])?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
