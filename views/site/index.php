<?php
$path=\Yii::getAlias('@app'). DIRECTORY_SEPARATOR . 'file_print';
$files = glob($path.'/*.pdf', GLOB_BRACE);
$school = new  \app\models\School();
?>
<div class="row">

    <!-- <div class="col-md-2">
        <H4>List file</H4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <td>#</td>
                <td>name file</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($files as$k=>$file):?>
                <tr>
                    <td><?=$k+1?></td>
                    <td>
                        <?php echo  substr($file,39)?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div> -->
    <img src="logo/nextgen.png">
    <div class="col-md-6">
        <div id="app">
            <h4> List data firebase ({{filteredOrderData.length}})</h4>
            <input type="text" name="name" class="form-control" placeholder="ค้นหา Order" title="" v-model="search">
            <table class="table table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th width="3%">order_id</th>
                        <th width="3%">status</th>
                        <th width="10%">date</th>
                        <th width="25%">menu</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(model,index) in filteredOrderData.slice(0,20)">
                        <td>{{index+1}}</td>
                        <td>{{model.order_id}}</td>
                        <td>{{model.status}}</td>
                        <td>{{model.create_at}}</td>
                        <td>
                            <!--                        <a href="#" @click="CopyOrder(model.id,model.ids)" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-copy"></i>Copy</a>-->
                            <a target="_blank" v-bind:href="'/site/copy?id='+ model.id"
                                class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-copy"></i>Copy</a>
                            <a href="#" @click="SendOrder(model.id,model.ids)" class="btn btn-info btn-xs"><i
                                    class="glyphicon glyphicon-refresh"></i>Reprint</a>
                            <a target="_blank" v-bind:href="'/site/addressed?id='+ model.id"
                                class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-send"></i> หน้าซอง</a>
                            <!-- <a href="#" @click="Letter(model.id)" class="btn btn-primary btn-xs"><i -->
                            <!-- class="glyphicon glyphicon-send"></i> หน้าซอง</a> -->
                            <a href="#" @click="editSend(model.id)" class="btn btn-default btn-xs"><i
                                    class="glyphicon glyphicon-pencil"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div id="app-replace">
            <h4> List data Replace ({{filteredOrderData.length}})</h4>
            <input type="text" name="name" class="form-control" placeholder="ค้นหา Order" title="" v-model="search">
            <table class="table table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th width="3%">Replace_id</th>
                        <th width="10%">status</th>
                        <th width="30%">DateTime</th>
                        <th width="10%">menu</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(model,index) in filteredOrderData.slice(0,20)">
                        <td>{{index+1}}</td>
                        <td>{{model.order_id}}</td>
                        <th>{{model.status}}</th>
                        <td>{{model.create_at}}</td>
                        <td>
                            <a href="#" @click="Reprint(model.id,model.ids)" class="btn btn-success btn-xs"><i
                                    class="glyphicon glyphicon-refresh"></i>Reprint</a>
                            <!-- <a target="_blank" v-bind:href="'/replace/index?id='+ model.id + '&key=' + model.ids"                                class="btn btn-success btn-xs"><i class="glyphicon glyphicon-refresh"></i>Reprint</a> -->
                            <a target="_blank" v-bind:href="'/replace/report-page?id='+ model.id"
                                class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-send"></i> หน้าซอง</a>
                            <!-- <a href="#" @click="Letter(model.id)" class="btn btn-primary btn-xs"><i -->
                            <!-- class="glyphicon glyphicon-send"></i> หน้าซอง</a> -->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>