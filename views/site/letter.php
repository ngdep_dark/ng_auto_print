<?php
$value = $company;
$address = [
    "name" => "บริษัท เนกเจนซอฟ จํากัด (สำนักงานใหญ่)",
    "address" => "เลขที่ 1558/41",
    "road" => "ถ.เทพรัตน",
    "sub_district" => "แขวงบางนาใต้",
    "district" => "เขตบางนา",
    "province" => "กรุงเทพฯ",
    "zip_code" => "10260",
    "phone" => "021014822",
    "tax_id" => "0105556004705"
];

$schoolAddress = explode(' ',$customer['address']);
$zipCode = $schoolAddress[count($schoolAddress) -1 ];
unset($schoolAddress[count($schoolAddress) -1]);
$schoolAddress = implode(' ',$schoolAddress);
?>

<div style="height: 50px">
    <div style="width: 5%;display: inline; float: left">
        <img src="img/NGLogo.jpg" style="width: 30px; height: 30px;">
    </div>
    <div style="display: inline; float: left">
        <span style="font-size: 20px; font-weight: Bold; font-style: Italic">NextSchool<span style="font-style: normal">
                <?= $address['name'] ?> </span></span><br>
    </div>
    <div>
        <span style="font-size: 20px"> <?= !empty($address['address'])?$address['address']:'-'?>
            <?= !empty($address['road'])?$address['road']:'-'?>
            <?=!empty($address['sub_district'])?$address['sub_district']:'-'?>
            <?=!empty($address['district'])?$address['district']:'-' ?></span><br>
        <span style="font-size: 20px">
            <?=!empty($address['province'])?$address['province']:'-' ?>&nbsp;<?= !empty($address['zip_code'])?$address['zip_code']:'-'?>&nbsp;
            <?=  !empty($address['phone'])?"โทร.".$address['phone']:'-'?></span>
    </div>
</div>
<br>
<div style="margin-left: 250px; margin-right: 20px; height: 150px;margin-top: 10px">
    <p style="font-size: 26px; margin-top: 0px; font-weight: bold;">กรุณาส่ง <br>
        <span style="font-size: 24px; font-weight: normal">
            <?= $customer['name']?>&nbsp;<span style="font-size: 24px; font-weight: bold">
                <?= 'โทร.'.$customer['phone'] ?> ( order#<?=$order?>)</span><br></span>
        <?php if(!empty($customer['school_name'])):?>
        <span style="font-size: 24px; font-weight: normal"> <?= $customer['school_name'] ?>&nbsp; <br></span>
        <?php endif;?>
        <?php if(!empty($customer['address'])):?>
        <span style="font-size: 24px; font-weight: normal"> <?= $schoolAddress?> </span> <br /> <span
            style="font-size: 26px; font-weight: bold"> <?=$zipCode?> </span>
        <?php endif;?>
    </p>
</div>