// Initialize Firebase
var config = {
  apiKey: "AIzaSyCJOffpaw38pOAoso6RcO-7-loP3ZJLFQ4",
  authDomain: "ngautoprint.firebaseapp.com",
  databaseURL: "https://ngautoprint.firebaseio.com",
  projectId: "ngautoprint",
  storageBucket: "ngautoprint.appspot.com",
  messagingSenderId: "889236489"
};

firebase.initializeApp(config);
const database = firebase.database();
const orderRef = database.ref("print-order");
const replaceRef = database.ref("print-replace");

var d = new Date();
const time = d.getTime();

new Vue({
  el: "#app",
  data: {
    text_input: "",
    orderData: [],
    search: ""
  },
  computed: {
    filteredOrderData() {
      var that = this;
      var search_data = this.orderData.reverse();
      searchString = this.search;
      if (!searchString) {
        return search_data;
      }
      searchString = searchString.trim().toLowerCase();
      search_data = search_data.filter(item => {
        if (
          !searchString ||
          item.order_id
            .toString()
            .toLowerCase()
            .indexOf(searchString) !== -1
        )
          return item;
      });
      return search_data.sort((a, b) => b.id - a.id);
    }
  },
  methods: {
    deleteData: function(data) {
      orderRef.child(data.ids).remove();
      // console.log(data);
    },

    printUrl: function(id) {
      axios
        .get("/site/report")
        .then(res => {
          // console.log(res)
        })
        .catch(e => {});
    },
    Reprint(data) {
      axios
        .get("/site/report?id=" + data)
        .then(res => {
          //location.reload();
        })
        .catch(e => {});
    },
    editSend: function(data) {
      return (location.href = "/site/edit-send?id=" + data);
      // console.log(data);
    },
    Letter(data) {
      return (location.href = "/site/addressed?id=" + data);
      // axios.get("/site/addressed?id="+data).then(res=>{
      //  console.log("Letter" +data)
      // }).catch(e=>{
      //
      // })
    },
    SendOrder(data, key) {
      const id = key;
      axios
        .get("/site/report?id=" + data + "&key=" + key)
        .then(res => {
          if (res.data === "not") {
            console.log("print =" + id);
            orderRef.child(id).update({ status: false });
          }
        })
        .catch(e => {});
    },
    CopyOrder(data, key) {
      const id = key;
      axios
        .get("/site/copy?id=" + data)
        .then(res => {
          if (res.data === "success") {
            console.log("copy success");
          }
        })
        .catch(e => {});
    }
  },
  created() {
    //event หลังจากเพิ่มข้อมูลเสร็จ
    orderRef.on("child_added", shanpshot => {
      //save to messages
      this.orderData.push({
        ...shanpshot.val(),
        ids: shanpshot.key
      });
      const order = shanpshot.val();
      const order_id = order.id;
      if (order.status) {
        // this.SendOrder(order_id,shanpshot.key);
        //   this.Letter(order_id);
        //console.log('print ='+order_id);
      }
    });
    orderRef.on("child_removed", shanpshot => {
      var deleteKey = this.orderData.find(value => value.ids == shanpshot.key);
      var index = this.orderData.indexOf(deleteKey);
      this.orderData.splice(index, 1); //delete array
    });
  }
});

new Vue({
  el: "#app-replace",
  data: {
    text_input: "",
    orderReplace: [],
    search: ""
  },
  computed: {
    filteredOrderData() {
      var that = this;
      var search_data = this.orderReplace.reverse();
      searchString = this.search;
      if (!searchString) {
        return search_data;
      }
      searchString = searchString.trim().toLowerCase();
      search_data = search_data.filter(item => {
        if (
          !searchString ||
          item.order_id
            .toString()
            .toLowerCase()
            .indexOf(searchString) !== -1
        )
          return item;
      });
      return search_data.sort((a, b) => b.id - a.id);
    }
  },
  methods: {
    GetReplace: function() {
      let that = this;

      // axios.get("/site/replace",).then(res=>{
      //     that.orderReplace.push(...res.data)
      // }).catch(e=>{

      // })
    },
    Reprint(id, key) {
      const key_id = key;
      const pathReplace = "/replace/index?id=" + id + "&key=" + key_id;
      axios
        .get(pathReplace)
        .then(res => {
          if (res.data === "not") {
            replaceRef.child(key_id).update({ status: false });
          }
        })
        .catch(e => {
          console.log(e);
        });
    },
    Letter(data) {
      return (location.href = "/replace/report-page?id=" + data);
      // axios.get("/site/addressed?id="+data).then(res=>{
      //  console.log("Letter" +data)
      // }).catch(e=>{
      //
      // })
    }
  },
  created() {
    //event หลังจากเพิ่มข้อมูลเสร็จ
    this.GetReplace();
    replaceRef.on("child_added", shanpshot => {
      //save to messages
      this.orderReplace.push({
        ...shanpshot.val(),
        ids: shanpshot.key
      });
    });
    console.log("ok");
  }
});
