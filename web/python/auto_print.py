import sys
import os
import cups
import time

# pathnow ="/home/snilli/Data/project/ng_management/file_print/"
pathnow = sys.argv[1]

# # cd to dir
path, dir, files = next(os.walk(pathnow))
file_count = len(files)
print_option = {}

if sys.argv[3] == 2:
    print_option = {'PageSize': '117x231mm'}

if file_count > 0:
    # connect printer
    conn = cups.Connection()
    # printer name
    printer = list(conn.getPrinters().keys())[0]
    # start print file then return job state
    printer_returns = conn.printFile(
        printer, sys.argv[1] + "/" + sys.argv[2], "statement financial", print_option)

    while True:
        printer_stat = conn.getJobAttributes(printer_returns)["job-state"]

        # print (printer_stat)

        if printer_stat == 9:
            os.remove(pathnow + "/" + sys.argv[2])
            print("ok")
            break
        else:
            time.sleep(2)
